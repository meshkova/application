package sample;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.*;

/**
 * Класс расчёта чаевых
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Controller {


    private double output_tip = 0;
    private double output_result = 0;
    private double output_result_in_rubles = 0;
    @FXML
    private Label cheque;

    @FXML
    private TextField inputBill;

    @FXML
    private Label error;

    @FXML
    private ComboBox<?> comboBox;

    @FXML
    private Label tip;

    @FXML
    private ToggleGroup group;

    @FXML
    private RadioButton noTip;

    @FXML
    private RadioButton fivePercent;

    @FXML
    private RadioButton tenPercent;

    @FXML
    private Button deem;

    @FXML
    private Button clear;

    @FXML
    private CheckBox inRubles;

    @FXML
    private CheckBox round;

    @FXML
    private Label resultInRubles;

    @FXML
    private TextField outputTip;

    @FXML
    private TextField outputBillInRubles;

    @FXML
    private TextField outputResult;

    @FXML
    private Label InRublesCheque;

    @FXML
    private TextField outputResultInRubles;

    @FXML
    private Label tipInRubles;

    @FXML
    private TextField outputTipInRubles;


    /**
     * Метод, скрывающий поля , очищающий поля, снимающий метки с RadioButton  и CheckBox
     *
     * @param event нажатие на Button (очистить)
     */
    @FXML
    void clickOnClear(ActionEvent event) {
        tipInRubles.setVisible(false);
        InRublesCheque.setVisible(false);
        resultInRubles.setVisible(false);
        outputBillInRubles.setVisible(false);
        outputResultInRubles.setVisible(false);
        outputTipInRubles.setVisible(false);
        inRubles.setSelected(false);
        round.setSelected(false);
        inputBill.clear();
        outputTip.clear();
        outputResult.clear();
        error.setVisible(false);
    }

    /**
     * Метод, предоставляющий возможность вычислить сумму чаевых и сумму к оплате
     *
     * @param event нажатие на Button (посчитать)
     */
    @FXML
    void clickOnDeem(ActionEvent event) {
        if (!isValidation()|| parses(inputBill) < 0) {
            outputError();
            return;
        }

        radioButtonChanged();
        clickOnInRubles(event);
        clickOnRound(event);
        error.setVisible(false);

    }


    /**
     * Метод, осуществляющий перевод из Долларов в рубли и из Евро в рубли
     *
     * @param event нажатие на CheckBox
     */
    @FXML
    void clickOnInRubles(ActionEvent event) {

        if (comboBox.getValue().equals("Рубли")) return;


        if (!isValidation()|| parses(inputBill) < 0) {
            outputError();
            return;
        }

        if (inRubles.isSelected()) {


            tipInRubles.setVisible(true);
            InRublesCheque.setVisible(true);
            resultInRubles.setVisible(true);
            outputBillInRubles.setVisible(true);
            outputResultInRubles.setVisible(true);
            outputTipInRubles.setVisible(true);

            double billInRubles = parses(inputBill);
            double tipInRubles = output_tip;

            double result_In_Rubles;

            if (comboBox.getValue().equals("Евро")) {
                billInRubles = billInRubles * 79.7775;
                tipInRubles = tipInRubles * 79.7775;
            }
            if (comboBox.getValue().equals("Доллары")) {
                billInRubles = billInRubles * 73.5819;
                tipInRubles = tipInRubles * 73.5819;

            }
            billInRubles = roundingNumber(billInRubles);
            outputBillInRubles.setText(String.valueOf(billInRubles));

            outputFormatted(outputBillInRubles);
            outputTipInRubles.setText(String.valueOf(tipInRubles));
            outputFormatted(outputTipInRubles);
            result_In_Rubles = billInRubles + tipInRubles;
            result_In_Rubles = roundingNumber(result_In_Rubles);
            outputResultInRubles.setText(String.valueOf(result_In_Rubles));
            output_result_in_rubles = parses(outputResultInRubles);
            outputFormatted(outputResultInRubles);


        } else {
            tipInRubles.setVisible(false);
            InRublesCheque.setVisible(false);
            resultInRubles.setVisible(false);
            outputBillInRubles.setVisible(false);
            outputResultInRubles.setVisible(false);
            outputTipInRubles.setVisible(false);
        }
    }


    /**
     * Метод, округляющий результат(к оплате) в валюте и в рублях
     * <p>
     * Если не нажата (isPressedButton) кнопка посчитать, то метод выполняться не будет
     * <p>
     *
     * @param event нажатие на CheckBox
     */
    @FXML
    void clickOnRound(ActionEvent event) {

        if (!isValidation() || parses(inputBill) < 0) {
            outputError();
            return;
        }


        double forPayment = output_result;

        if (round.isSelected()) {

            if (group.getSelectedToggle().equals(noTip)) {
                forPayment = Math.ceil(forPayment);

            }

            if (!group.getSelectedToggle().equals(noTip)) {
                forPayment = Math.round(forPayment);

            }
        }
        outputResult.setText(String.valueOf(forPayment));
        outputFormatted(outputResult);
        roundingUP();

    }


    /**
     * Метод, позволяющий определить
     * какая RadioButton выбрана
     * <p>
     * Если поле ввода суммы чека, меньше нуля, то будет вызван метод, сообщающий о ошибке
     */
    private void radioButtonChanged() {

        if (isValidation()) {
            double tipsInMoney = parses(inputBill);
            if (group.getSelectedToggle().equals(noTip)) {
                tipsInMoney = 0;
            }

            if (group.getSelectedToggle().equals(fivePercent)) {
                tipsInMoney = tipsInMoney / 100 * 5;
            }

            if (group.getSelectedToggle().equals(tenPercent)) {
                tipsInMoney = tipsInMoney / 100 * 10;

            }
            tipsInMoney = roundingNumber(tipsInMoney);
            outputTip.setText(String.valueOf(tipsInMoney));
            output_tip = parses(outputTip);
            outputFormatted(outputTip);
            double forPayment = tipsInMoney + parses(inputBill);
            forPayment = roundingNumber(forPayment);

            outputResult.setText(String.valueOf(forPayment));
            output_result = parses(outputResult);
            outputFormatted(outputResult);
        }
    }

    /**
     * Метд, осуществляющий парсинг поля в вещественный тип
     *
     * @param textField поле
     * @return поле для ввода вещественных чисел
     */
    double parses(TextField textField) {
        return Double.valueOf(textField.getText());
    }

    /**
     * Метод, проверяющий число на валидность
     *
     */
   boolean isValidation() {
        try {
            parses(inputBill);
            return true;
        } catch (Exception e) {
            return false;

        }
    }


    /**
     * Метод, реагирующий на ошибку ввода
     */
    private void outputError() {
        error.setVisible(true);
        error.setText("Некорректный ввод");

    }

    /**
     * Метод, выводящий число в определенном формате
     *
     * @param number число
     * @return число типа double с 2 знаками после точки
     */
    private double roundingNumber(Double number) {

        return new BigDecimal(number).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    /**
     * Метод, округляющий число вверх
     */
    private void roundingUP() {
        double forPaymentInRubles = output_result_in_rubles;
        if (!inRubles.isSelected()) return;
        if (round.isSelected() == true) {
            if (group.getSelectedToggle().equals(noTip)) {
                forPaymentInRubles = Math.ceil(forPaymentInRubles);
            } else {
                forPaymentInRubles = Math.round(forPaymentInRubles);
            }

        }
        outputResultInRubles.setText(String.valueOf(forPaymentInRubles));
        outputFormatted(outputResultInRubles);

    }


    /**
     * Метод, форматирующй вывод числа
     * с двумя знаками после запятой
     *
     * @param textField поле
     */
    private void outputFormatted(TextField textField) {
        DecimalFormat formatTwo = new DecimalFormat("#0.00");
        double number1 = Double.valueOf(textField.getText());

        String str1 = formatTwo.format(number1);
        textField.setText(str1);
    }


}

